//
//  Tools.h
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <Foundation/Foundation.h>

#define mustOverride() @throw [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"%s must be overridden in a subclass/category", __PRETTY_FUNCTION__] userInfo:nil]
#define methodNotImplemented() mustOverride()

@interface Tools : NSObject

+(NSString*) secondsToTimeStr:(float) totalSeconds;
+(NSString*) secondsToTimeStr:(float) totalSeconds withTens:(BOOL) tens;

@end
