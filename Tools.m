//
//  Tools.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "Tools.h"

@implementation Tools

+(NSString*) secondsToTimeStr:(float) totalSeconds
{
    return [Tools secondsToTimeStr:totalSeconds withTens:NO];
}

+(NSString*) secondsToTimeStr:(float) totalSeconds withTens:(BOOL) tens
{
    int _seconds = (int) totalSeconds % 60;
    int _minutes = (int)(totalSeconds / 60) % 60;
    int _hours   = (int) totalSeconds / 3600;
    int _tens    = (totalSeconds - ((int)totalSeconds)) * 100;
   
    if (tens) {
        if (_hours == 0)
            return [NSString stringWithFormat:@"%02d:%02d.%02d", _minutes, _seconds, _tens];
        else
            return [NSString stringWithFormat:@"%02d:%02d:%02d.%02d", _hours, _minutes, _seconds, _tens];
    } else {
        if (_hours == 0)
            return [NSString stringWithFormat:@"%02d:%02d", _minutes, _seconds];
        else
            return [NSString stringWithFormat:@"%02d:%02d:%02d", _hours, _minutes, _seconds];
    }
}

@end
