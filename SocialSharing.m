//
//  SocialSharing.m
//  ApneaTimerHD
//
//  Created by Mikhail on 20.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Social/Social.h>
#import <Accounts/Accounts.h>

#import "SocialSharing.h"
#import "Flurry.h"

@implementation SocialSharing

+(NSString*) getShareLink:(NSString*)appid {
    return [NSString stringWithFormat:@"http://itunes.apple.com/app/%@", appid];
}

+(void) shareOnTwitter:(id<MessageBoxDelegate>)delegate andLink:(NSString*)link andMessage:(NSString*)message {
    ACAccountStore *account    = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [account requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        if (granted == YES) {
            NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
            if ([arrayOfAccounts count] > 0) {
                ACAccount *twitterAccount = [arrayOfAccounts lastObject];
                NSDictionary *msg = @{@"status":[NSString stringWithFormat:@"%@ (%@)", message, link]};
                NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
                SLRequest *postRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                            requestMethod:SLRequestMethodPOST
                                                                      URL:requestURL parameters:msg];
                postRequest.account = twitterAccount;
                [postRequest performRequestWithHandler:^(NSData            *responseData,
                                                         NSHTTPURLResponse *urlResponse,
                                                         NSError           *error) {
                    if ([urlResponse statusCode] == 200) {
                        if (delegate)
                            [delegate messageBox:NSLocalizedString(@"shared this app on twitter", nil)
                                       withTitle:NSLocalizedString(@"done", nil)
                                        needShow:NO];
                            [Flurry logEvent:@"share_twitter"];
                    } else {
                        if (delegate)
                            [delegate messageBox:NSLocalizedString(@"error sharing on twitter", nil)
                                       withTitle:NSLocalizedString(@"error", nil)
                                        needShow:YES];
                    }
                }];
            } else {
                if (delegate)
                    [delegate messageBox:NSLocalizedString(@"twitter account not set", nil)
                               withTitle:NSLocalizedString(@"error", nil)
                                needShow:YES];
            }
        } else {
            if (delegate)
                [delegate messageBox:NSLocalizedString(@"can't connect to twitter", nil)
                           withTitle:NSLocalizedString(@"error", nil)
                            needShow:YES];
        }
    }];
}

+(void) shareOnFacebook:(id<MessageBoxDelegate>)delegate andLink:(NSString*)link andMessage:(NSString*)message {
    ACAccountStore *accountStore        = [[ACAccountStore alloc] init];
    NSDictionary *emailReadPermisson    = @{
                                            ACFacebookAppIdKey : @"431683316987583",
                                            ACFacebookPermissionsKey : @[@"email"],
                                            ACFacebookAudienceKey : ACFacebookAudienceFriends,
                                           };
    NSDictionary *publishWritePermisson = @{
                                            ACFacebookAppIdKey : @"431683316987583",
                                            ACFacebookPermissionsKey : @[@"publish_actions"],
                                            ACFacebookAudienceKey : ACFacebookAudienceFriends
                                            };
    ACAccountType *facebookAccountType = [accountStore
                                          accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    //Request for Read permission
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:emailReadPermisson completion:^(BOOL granted, NSError *error) {
        if (granted) {
            //Request for write permission
            [accountStore requestAccessToAccountsWithType:facebookAccountType options:publishWritePermisson completion:^(BOOL granted, NSError *error) {
                if (granted) {
                    NSArray *accounts = [accountStore
                                         accountsWithAccountType:facebookAccountType];
                    if ([accounts count] > 0) {
                        [self fbPostLink:link
                             withAccount:[accounts lastObject]
                             andDelegate:delegate
                              andMessage:message];
                        
                    } else {
                        if (delegate)
                            [delegate messageBox:NSLocalizedString(@"no facebook account configured", nil)
                                       withTitle:NSLocalizedString(@"error", nil)
                                        needShow:YES];
                    }
                } else {
                    if (delegate)
                        [delegate messageBox:NSLocalizedString(@"no write access is granted", nil)
                                   withTitle:NSLocalizedString(@"error", nil)
                                    needShow:YES];
                }
            }];
        } else {
            if (delegate)
                [delegate messageBox:NSLocalizedString(@"no read access is granted", nil)
                           withTitle:NSLocalizedString(@"error", nil)
                            needShow:YES];
        }
    }];
}

+(void) fbPostLink:(NSString*) link withAccount:(ACAccount*)account
                                    andDelegate:(id<MessageBoxDelegate>)delegate
                                     andMessage:(NSString*)message {
//    NSLog(@"Successfull access for account: %@", facebookAccount.username);
    NSURL *feedURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
    NSDictionary *parameters = @{
                                 @"message": message,
                                 @"link" : link,
                                };
    SLRequest *feedRequest = [SLRequest
                              requestForServiceType:SLServiceTypeFacebook
                              requestMethod:SLRequestMethodPOST
                              URL:feedURL
                              parameters:parameters];
    feedRequest.account = account;
    [feedRequest performRequestWithHandler:^(NSData *responseData,
                                             NSHTTPURLResponse *urlResponse, NSError *error) {
        if ([urlResponse statusCode] == 200) {
            if (delegate)
                [delegate messageBox:NSLocalizedString(@"shared this app on facebook", nil)
                           withTitle:NSLocalizedString(@"done", nil)
                            needShow:NO];
            [Flurry logEvent:@"share_facebook"];
        } else {
            if (delegate)
                [delegate messageBox:NSLocalizedString(@"error sharing on facebook", nil)
                           withTitle:NSLocalizedString(@"error", nil)
                            needShow:YES];
        }
//        NSLog(@"Response: %@", [urlResponse description]);
    }];
}

@end
