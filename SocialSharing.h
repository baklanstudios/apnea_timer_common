//
//  SocialSharing.h
//  ApneaTimerHD
//
//  Created by Mikhail on 20.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MessageBoxDelegate /*<NSObject>*/

-(void) messageBox:(NSString*)message withTitle:(NSString*)title needShow:(BOOL) needShow ;

@end


@interface SocialSharing : NSObject

//+(void) shareOnTwitter:(id<MessageBoxDelegate>)delegate  andAppId:(NSString*)appid andMessage:(NSString*)message;
//+(void) shareOnFacebook:(id<MessageBoxDelegate>)delegate andAppId:(NSString*)appid andMessage:(NSString*)message;

+(void) shareOnTwitter:(id<MessageBoxDelegate>)delegate  andLink:(NSString*)link andMessage:(NSString*)message;
+(void) shareOnFacebook:(id<MessageBoxDelegate>)delegate andLink:(NSString*)link andMessage:(NSString*)message;

@end
